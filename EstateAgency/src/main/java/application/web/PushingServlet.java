package application.web;

import application.domain.Agency;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class PushingServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] phonesToPush = request.getParameterValues("phones");
        Agency agency = Agency.getInstance();
        if (phonesToPush != null) {
            for (String phone : phonesToPush) {
                agency.pushToTransaction(phone);
            }
        }
        request.setAttribute("apartmentsToVerify", agency.getApartmentsToVerify());
        request.setAttribute("verifiedApartments", agency.getVerifiedApartments());
        request.setAttribute("apartmentsInTransaction", agency.getApartmentsInTransaction());
        request.getRequestDispatcher("WEB-INF/pages/registration.jsp").forward(request, response);
    }
}
