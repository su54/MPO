package application.web;

import application.domain.Agency;
import application.domain.Apartment;
import application.domain.Furnishings;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UpdatingServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //update apartment data in any table
        Agency agency = Agency.getInstance();
        List<String> issueList = new ArrayList<>();
        Apartment sample = new Apartment();
        String priceString = request.getParameter("price");
        if (!priceString.equals("")) {
        	try {
                sample.setPrice(Integer.parseInt(priceString));
            } catch (NumberFormatException ex) {
                issueList.add("invalid price input");
            }
        }
        else {
        	sample.setPrice(1);
        }
        String totalAreaString = request.getParameter("totalArea");
        if (!totalAreaString.equals("")) {
        	try {
                sample.setTotalArea(Float.parseFloat(totalAreaString));
            } catch (NumberFormatException ex) {
                issueList.add("invalid total area input");
            }
        }
        else {
        	sample.setTotalArea(1);
        }
        String livingSpaceString = request.getParameter("livingSpace");
        if (!livingSpaceString.equals("")) {
        	try {
                sample.setLivingSpace(Float.parseFloat(livingSpaceString));
            } catch (NumberFormatException ex) {
                issueList.add("invalid living space input");
            }
        }
        else {
        	sample.setLivingSpace(1);
        }
        String roomNumberString = request.getParameter("roomNumber");
        if (!roomNumberString.equals("")) {
        	try {
                sample.setRoomNumber(Integer.parseInt(roomNumberString));
            } catch (NumberFormatException ex) {
                issueList.add("invalid room number input");
            }
        }
        else {
        	sample.setRoomNumber(1);
        }
        String storeyString = request.getParameter("storey");
        if (!storeyString.equals("")) {
        	try {
                sample.setStorey(Integer.parseInt(storeyString));
            } catch (NumberFormatException ex) {
                issueList.add("invalid storey input");
            }
        }
        else {
        	sample.setStorey(1);
        }
        switch (request.getParameter("furnishingsType")) {
            case "none":
                sample.setFurnishings(Furnishings.NONE);
                break;
            case "partial":
                sample.setFurnishings(Furnishings.PARTIAL);
                break;
            case "full":
                sample.setFurnishings(Furnishings.FULL);
                break;
        }
        String address = request.getParameter("address");
        if (!address.equals("")) {
            sample.setAddress(address);
        }
        else {
            sample.setAddress("1");
        }
        String phone = request.getParameter("thePhone");
        if (!phone.equals("")) {
            sample.setPhone(phone);
        }
        else {
            issueList.add("invalid phone");
        }
        if (issueList.isEmpty()) {
            try {
                agency.updateBySample(sample);
            } catch (RuntimeException ex) {
                issueList.add(ex.getMessage());
            }
            finally {
            	request.setAttribute("apartmentsToVerify", agency.getApartmentsToVerify());
                request.setAttribute("verifiedApartments", agency.getVerifiedApartments());
                request.setAttribute("apartmentsInTransaction", agency.getApartmentsInTransaction());
                request.setAttribute("issueList", issueList);
                request.getRequestDispatcher("WEB-INF/pages/registration.jsp").forward(request, response);
            }
        }
        else {
        	request.setAttribute("apartmentsToVerify", agency.getApartmentsToVerify());
            request.setAttribute("verifiedApartments", agency.getVerifiedApartments());
            request.setAttribute("apartmentsInTransaction", agency.getApartmentsInTransaction());
            request.setAttribute("issueList", issueList);
            request.getRequestDispatcher("WEB-INF/pages/registration.jsp").forward(request, response);
        }
    }
}
