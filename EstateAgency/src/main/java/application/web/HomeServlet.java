package application.web;

import application.domain.Agency;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HomeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Agency agency = Agency.getInstance();
        agency.registerEntering();
        request.getRequestDispatcher("WEB-INF/pages/home.jsp").forward(request, response);
    }
}