package application.web;

import application.domain.Agency;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CancellationServlet extends HomeServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //return apartments from transaction back to verified list
        String[] phonesToCancel = request.getParameterValues("phones");
        Agency agency = Agency.getInstance();
        if (phonesToCancel != null) {
            for (String phone : phonesToCancel) {
                agency.cancelTransaction(phone);
            }
        }
        request.setAttribute("apartmentsToVerify", agency.getApartmentsToVerify());
        request.setAttribute("verifiedApartments", agency.getVerifiedApartments());
        request.setAttribute("apartmentsInTransaction", agency.getApartmentsInTransaction());
        request.getRequestDispatcher("WEB-INF/pages/registration.jsp").forward(request, response);
    }
}
