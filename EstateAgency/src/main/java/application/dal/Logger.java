package application.dal;

import application.domain.Role;

import java.sql.*;
import java.time.OffsetDateTime;

public class Logger extends Dao {

	public Logger() {
	}

	public void addLog() {
		try {
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement("insert into " + logTable + " (date) values (?)");
			OffsetDateTime date = OffsetDateTime.now();
			statement.setObject(1, date);
			statement.execute();
			statement.close();
			connection.close();
		} catch (SQLException ex) {
			throw new RuntimeException("enable to add authorization log", ex);
		}
	}

	public void recordTransactionOnPhone(String phone) {
		try {
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement("insert into " + completedTransactionsTable
					+ " (contact_phone, price, address) " + "select contact_phone, price, address from "
					+ transactionAptTable + " where contact_phone = ?");
			statement.setString(1, phone);
			statement.execute();
			statement.close();
			PreparedStatement eraseStatement = connection
					.prepareStatement("delete from " + transactionAptTable + " where contact_phone = ?");
			eraseStatement.setString(1, phone);
			eraseStatement.execute();
			connection.close();
		} catch (SQLException ex) {
			throw new RuntimeException("enable to record transaction", ex);
		}
	}

	public boolean doesAccept(Role role) {
		try {
			Connection connection = getConnection();
			PreparedStatement statement = connection
					.prepareStatement("select * from " + credentialsTable + " where (login = ? and password = ?)");
			statement.setString(1, role.getLogin());
			statement.setString(2, role.getPassword());
			ResultSet resultSet = statement.executeQuery();
			boolean credentials = resultSet.next();
			resultSet.close();
			statement.close();
			connection.close();
			return credentials;
		} catch (SQLException ex) {
			throw new RuntimeException("enable to verify credentials", ex);
		}
	}

}
