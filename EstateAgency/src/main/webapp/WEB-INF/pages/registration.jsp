<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>Registration</title>
    <style>
        body {
            background-color: aliceblue;
            font-family: calibri;
        }
        .tbl_main {
            background-color: white;
            border-radius: 10px;
            border-spacing: 0;
            border-collapse: separate;
            text-align: center;
            width:100%;
        }
        .tbl_upd {
            text-align: left;
            background-color: white;
            border-radius: 10px;
            border-spacing: 0;
            border-collapse: separate;
            width:100%;
        }
        .header_left {
            background-color: lightblue;
            border-radius: 10px 0 0 0;
        }
        .header_right {
            background-color: lightblue;
            border-radius: 0 10px 0 0;
        }
        .button_submit {
            border-radius:5px;
            background-color:lightblue;
            width: 75px;
        }
        a:link {
            text-decoration: none;
        }
        a:hover {
            text-decoration: underline;
        }

    </style>
</head>
<body>
<table class="tbl_main">
    <tr>
        <th class="header_left">Applications</th>
        <th style="background-color: lightblue">Verified</th>
        <th class="header_right">In Transaction</th>
    </tr>
    <td>
        <form action="verification" method="post" style="text-align: left">
            <ol>
                <c:forEach var="apartment" items="${apartmentsToVerify}">
                    <li  style="text-align: left">
                        <input type="checkbox" id="elementToVerify"
                               name="phones" value="${apartment.getPhone()}">
                        <label for="elementToVerify">${apartment.toString()}</label>
                    </li>
                </c:forEach>
            </ol>
            <a>
                <button type="submit" class="button_submit">verify</button>
                <button type="submit" formaction="removing"
                        class="button_submit">remove
                </button>
            </a>
        </form>
    </td>
    <td>
        <form action="pushing" method="post" style="text-align: left">
            <ol>
                <c:forEach var="apartment" items="${verifiedApartments}">
                    <li  style="text-align: left">
                        <input type="checkbox" id="elementToPush"
                               name="phones" value="${apartment.getPhone()}">
                        <label for="elementToPush">${apartment.toString()}</label>
                    </li>
                </c:forEach>
            </ol>
            <a>
                <button type="submit" class="button_submit">push</button>
                <button type="submit" formaction="removing"
                        class="button_submit">forget
                </button>
            </a>
        </form>
    </td>
    <td>
        <form action="completing" method="post" style="text-align: left">
            <ol>
                <c:forEach var="apartment" items="${apartmentsInTransaction}">
                    <li  style="text-align: left">
                        <input type="checkbox" id="elementInTransaction"
                               name="phones" value="${apartment.getPhone()}">
                        <label for="elementInTransaction">${apartment.toString()}</label>
                    </li>
                </c:forEach>
            </ol>
            <a>
                <button type="submit" class="button_submit">complete</button>
                <button type="submit" formaction="cancellation"
                        class="button_submit">cancel
                </button>
            </a>
        </form>
    </td>
    <tr>
    </tr>
</table>

<br/>

<table style="width:40%">
    <tr>
        <td style="width:65%">
            <div>
                <form action="updating" method="post">
                    <table class="tbl_upd">
                        <tr>
                            <td class="header_left">
                                <b>Update on phone:</b>
                            </td>
                            <td class="header_right">
                                <input name="thePhone" style="border-radius:5px; width:90%">
                            </td>
                        </tr>
                        <tr>
                            <td>Price:</td>
                            <td>
                                <input name="price"
                                       type="number" step="1000"
                                       style="border-radius:5px; width:90%">
                            </td>
                        </tr>
                        <tr>
                            <td>Total area:</td>
                            <td>
                                <input name="totalArea"
                                       type="number" step="0.01"
                                       style="border-radius:5px; width:90%">
                            </td>
                        </tr>
                        <tr>
                            <td>Living space:</td>
                            <td>
                                <input name="livingSpace"
                                       type="number" step="0.01"
                                       style="border-radius:5px; width:90%">
                            </td>
                        </tr>
                        <tr>
                            <td>Room number:</td>
                            <td>
                                <input name="roomNumber"
                                       type="number" step="1"
                                       style="border-radius:5px; width:90%">
                            </td>
                        </tr>
                        <tr>
                            <td>Storey:</td>
                            <td>
                                <input name="storey"
                                       type="number" step="1"
                                       style="border-radius:5px; width:90%">
                            </td>
                        </tr>
                        <tr>
                            <td>Furnishings:</td>
                            <td>
                                <select name="furnishingsType"
                                        style="border-radius:5px; width:92%">
                                    <option name="NONE">none</option>
                                    <option name="PARTIAL">partial</option>
                                    <option name="FULL">full</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Address:</td>
                            <td>
                                <input name="address" style="border-radius:5px; width:90%">
                            </td>
                        </tr>
                    </table>
                    <button type="submit" class="button_submit">update</button>
                </form>
            </div>
        </td>
        <td style="width:35%">
            <div style="color: red; text-align: right">
                <c:if test="${issueList.isEmpty() == false}">
                    <c:out value="Warning:"></c:out>
                    <c:out value="${issueList.toString()}"></c:out>
                </c:if>
            </div>
        </td>
    </tr>
</table>
<br/>
<div style="color: lightblue; text-align: center">
    <a href="${pageContext.request.contextPath}/home" style="color: teal">To homepage</a>
     ||
    <a href="${pageContext.request.contextPath}/searching" style="color: teal">To searching</a>
     ||
    <a href="${pageContext.request.contextPath}/application" style="color: teal">To application form</a>
    <br/>
    <c style="color: teal">Our contact phone: +44 *** **** ***</c>
</div>
</body>
</html>