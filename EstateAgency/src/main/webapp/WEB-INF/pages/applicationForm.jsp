<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>Application</title>
    <style>
        body {
            background-color: aliceblue;
            font-family: calibri;
        }
        .tbl_main {
            background-color: white;
            border-radius: 10px;
            border-spacing: 0;
            border-collapse: separate;
            text-align: center;
            width:100%;
        }
        .tbl_upd {
            background-color: white;
            border-radius: 10px;
            border-spacing: 0;
            border-collapse: separate;
            text-align: center;
            width:100%;
        }
        .header_left {
            background-color: lightblue;
            border-radius: 10px 0 0 0;
        }
        .header_right {
            background-color: lightblue;
            border-radius: 0 10px 0 0;
        }
        .button_submit {
            border-radius:5px;
            background-color:lightblue;
            width: 75px;
        }
        a:link {
            text-decoration: none;
        }
        a:hover {
            text-decoration: underline;
        }
    </style>
</head>
<body>
<h2 style="text-align:center; color:teal">Application Form</h2>
<h4 style="text-align:left; color:teal">Fill the form below:</h4>

<form action="application" method="post">
    <table class="tbl_main" style="width:30%">
        <tr>
            <th class="header_left">
                Parameter
            </th>
            <th class="header_right">
                Value
            </th>
        </tr>
        <tr>
            <td>Price:</td>
            <td>
                <input name="price" style="border-radius:5px; width:90%"
                       type="number" step="1000">
            </td>
        <tr>
            <td>Total area:</td>
            <td>
                <input name="totalArea" style="border-radius:5px; width:90%"
                       type="number" step="0.01">
            </td>
        </tr>
        <tr>
            <td>Living space:</td>
            <td>
                <input name="livingSpace" style="border-radius:5px; width:90%"
                       type="number" step="0.01">
            </td>
        </tr>
        <tr>
            <td>Room number:</td>
            <td>
                <input name="roomNumber" style="border-radius:5px; width:90%"
                       type="number" step="1">
            </td>
        </tr>
        <tr>
            <td>Storey:</td>
            <td>
                <input name="storey"
                       type="number" step="1"
                       style="border-radius:5px; width:90%">
            </td>
        </tr>
        <tr>
            <td>Furnishings:</td>
            <td>
                <select name="furnishingsType"
                        style="border-radius:5px; width:92%">
                    <option name="NONE">none</option>
                    <option name="PARTIAL">partial</option>
                    <option name="FULL">full</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Address:</td>
            <td>
                <input name="address" style="border-radius:5px; width:90%">
            </td>
        </tr>
        <tr>
            <td>Contact phone:</td>
            <td>
                <input name="phoneNumber" style="border-radius:5px; width:90%">
            </td>
        </tr>
    </table>
    <button type="submit" class="button_submit">apply</button>
</form>
</body>
<br/>
<div style="color: red; margin-left: 25%">
    <c:if test="${issueList.isEmpty() == false}">
        <c:out value="Warning:"></c:out>
        <c:out value="${issueList.toString()}"></c:out>
    </c:if>
</div>
<div style="color: lightblue; text-align: center">
    <a href="${pageContext.request.contextPath}/" style="color: teal">To homepage</a>
    ||
    <a href="${pageContext.request.contextPath}/searching" style="color: teal">To searching</a>
    ||
    <a href="${pageContext.request.contextPath}/application" style="color: teal">To application form</a>
    <br/>
    <c style="color: teal">Our contact phone: +44 *** **** ***</c>
</div>
</html>