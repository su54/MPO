<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<title>Search</title>
<style>
body {
	background-color: aliceblue;
	font-family: calibri;
}

.tbl_main {
	background-color: white;
	border-radius: 10px;
	border-spacing: 0;
	border-collapse: separate;
	text-align: center;
	width: 100%;
}

.header_left {
	background-color: lightblue;
	border-radius: 10px 0 0 0;
}

.header_right {
	background-color: lightblue;
	border-radius: 0 10px 0 0;
}

.button_submit {
	border-radius: 5px;
	background-color: lightblue;
	width: 75px;
}

a:link {
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
}
</style>
</head>
<body>
	<h2 style="text-align: center; color: teal">Search</h2>
	<h4 style="text-align: left; color: teal">Specify property search
		parameters:</h4>

	<form action="searching" method="post">
		<table class="tbl_main" style="width: 30%">
			<tr>
				<th class="header_left">Parameter</th>
				<th colspan="3" class="header_right">Value</th>
			</tr>
			<tr>
				<td>Min price:</td>
				<td><input name="minPrice"
					style="border-radius: 5px; width: 90%" type="number" step="1000">
				</td>
				<td>Max price</td>
				<td><input name="maxPrice"
					style="border-radius: 5px; width: 90%" type="number" step="1000">
				</td>
			</tr>
			<tr>
				<td>Min area:</td>
				<td><input name="minTotalArea"
					style="border-radius: 5px; width: 90%" type="number" step="0.01">
				</td>
				<td>Max area:</td>
				<td><input name="maxTotalArea"
					style="border-radius: 5px; width: 90%" type="number" step="0.01">
				</td>
			</tr>
			<tr>
				<td>Room number:</td>
				<td><input name="roomNumber"
					style="border-radius: 5px; width: 90%" type="number" step="1">
				</td>
				<td>Furnishings:</td>
				<td><select name="furnishingsType"
					style="border-radius: 5px; width: 93%">
						<option name="NONE">none</option>
						<option name="PARTIAL">partial</option>
						<option name="FULL">full</option>
				</select></td>
			</tr>
		</table>
		<br /> <b style="color: teal">Sort results by</b> <select
			name="sortOrder" style="border-radius: 5px; width: 75px">
			<option name="price">price</option>
			<option name="total_area">area</option>
		</select>
		<button type="submit" class="button_submit">search</button>
	</form>

	<div style="color: red; margin-left: 25%">
		<c:if test="${issueList.isEmpty() == false}">
			<c:out value="Warning:"></c:out>
			<c:out value="${issueList.toString()}"></c:out>
		</c:if>
	</div>

	<div style="color: lightblue; text-align: center">
		<a href="${pageContext.request.contextPath}/" style="color: teal">To
			homepage</a> || <a href="${pageContext.request.contextPath}/searching"
			style="color: teal">To searching</a> || <a
			href="${pageContext.request.contextPath}/application"
			style="color: teal">To application form</a>
			<br/>
    <c style="color: teal">Our contact phone: +44 *** **** ***</c>
	</div>
</body>
</html>