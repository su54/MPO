<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<title>Agency</title>
<style>
    body {
        background-color: aliceblue;
        font-family: calibri;
    }
    .tbl_main {
        background-color: white;
        border-radius: 10px;
        border-spacing: 0;
        border-collapse: separate;
        text-align: center;
        width:40%;
    }
    .header_left {
        background-color: lightblue;
        border-radius: 10px 0 0 0;
    }
    .header_right {
        background-color: lightblue;
        border-radius: 0 10px 0 0;
    }
    .button_submit {
        border-radius:5px;
        background-color:lightblue;
        width: 75px;
    }
    a:link {
        text-decoration: none;
    }
    a:hover {
        text-decoration: underline;
    }
</style>
<body>
<h1 style="text-align:center; color:teal">Portsmouth Estate Agency</h1>
<h4 style="text-align:left; color:teal">Contact phone: +44 *** **** ***</h4>
<form action="registration" method="post">
    <table class="tbl_main">
        <tr>
            <th class="header_left" style="width:30%">For Customers</th>
            <th colspan="2" class="header_right">For Staff Only</th>
        </tr>
        <tr>
            <td>
                <a href="${pageContext.request.contextPath}/searching" style="color: teal">Go search</a>
            </td>
            <td>Login:</td>
            <td>
                <input name="login" style="border-radius:5px; width:90%">
            </td>
        </tr>
        <tr>
            <td>
                <a href="${pageContext.request.contextPath}/application" style="color: teal">Go apply</a>
            </td>
            <td>Password:</td>
            <td>
                <input name="password" type="password" style="border-radius:5px; width:90%">
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                <button type="submit" class="button_submit">sign in</button>
            </td>
        </tr>
    </table>
</form>
<div style="color: red; margin-left: 25%">
    <c:if test="${issueList.isEmpty() == false}">
        <c:out value="Warning:"></c:out>
        <c:out value="${issueList.toString()}"></c:out>
    </c:if>
</div>
</body>
</html>