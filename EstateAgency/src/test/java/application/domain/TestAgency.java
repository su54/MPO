package application.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestAgency {
	private final Agency agency = Agency.getInstance();
	
	@Test
	public void testPhoneContentsOnlyNumbers() {
		assertTrue(agency.isContactPhoneValid("995123123123"));
	}
	
	@Test
	public void testStandardPhone() {
		assertTrue(agency.isContactPhoneValid("+44 123 4567 901"));
	}

	@Test
	public void testPhoneContentsSpaces() {
		assertTrue(agency.isContactPhoneValid("+ 995 123 123 123"));
	}
	
	@Test
	public void testPhoneContentsOnlyNotDigits() {
		assertFalse(agency.isContactPhoneValid("+ afg 1234 sd123f"));
	}
	
	@Test
	public void testPhoneContentsLetters() {
		assertFalse(agency.isContactPhoneValid("+ phone n2umber"));
	}
	
	@Test
	public void testPhoneContentsLettersAndDigits() {
		assertFalse(agency.isContactPhoneValid("+ asdfasd 2122"));
	}
}
